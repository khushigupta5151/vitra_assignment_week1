var bookmarks = [];
var formdata;
function submitForm(event) {
  const form = document.querySelector("form");
  formdata = Object.values(form).reduce((obj, field) => {
    obj[field.name] = field.value;
    return obj;
  }, {});
  console.log("formdata", formdata);
  bookmarks.push(formdata);
  console.log("bookmarks", bookmarks);
  event.preventDefault();
  printdata();
  return false;
}
function printdata() {
  //obtaining the table object
  var table = document.getElementById("mytable");
  //adding row to the last
  var row = table.insertRow(-1);
  //for iterating over properties of object
  for (let data in formdata) {
    if (data != "submit") {
      if (data == "url")
        row.insertCell(
          -1
        ).innerHTML = `<a href="${formdata[data]}">${formdata[data]}</a>`;
      else row.insertCell(-1).innerHTML = formdata[data];
      // console.log(formdata[data]);
    }
  }
}
